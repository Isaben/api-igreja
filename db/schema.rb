# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171012231928) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "congregacoes", force: :cascade do |t|
    t.string   "nome"
    t.string   "tipo"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "igreja_id"
    t.index ["igreja_id"], name: "index_congregacoes_on_igreja_id", using: :btree
  end

  create_table "enderecos", force: :cascade do |t|
    t.string   "rua"
    t.integer  "numero"
    t.string   "bairro"
    t.string   "cidade"
    t.string   "estado"
    t.string   "pais"
    t.string   "cep"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "igrejas", force: :cascade do |t|
    t.string   "nome"
    t.string   "ministerio"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "lider_id"
    t.integer  "congregacoes_id"
    t.integer  "tesoureiro_id"
    t.integer  "secretario_id"
    t.integer  "endereco_id"
    t.index ["congregacoes_id"], name: "index_igrejas_on_congregacoes_id", using: :btree
    t.index ["endereco_id"], name: "index_igrejas_on_endereco_id", using: :btree
    t.index ["lider_id"], name: "index_igrejas_on_lider_id", using: :btree
    t.index ["secretario_id"], name: "index_igrejas_on_secretario_id", using: :btree
    t.index ["tesoureiro_id"], name: "index_igrejas_on_tesoureiro_id", using: :btree
  end

  create_table "membros", force: :cascade do |t|
    t.string   "nome"
    t.string   "funcao"
    t.string   "telefone"
    t.string   "email"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.integer  "congregacao_id"
    t.integer  "endereco_id"
    t.integer  "igreja_id"
    t.index ["congregacao_id"], name: "index_membros_on_congregacao_id", using: :btree
    t.index ["endereco_id"], name: "index_membros_on_endereco_id", using: :btree
    t.index ["igreja_id"], name: "index_membros_on_igreja_id", using: :btree
  end

  add_foreign_key "igrejas", "membros", column: "secretario_id"
end
