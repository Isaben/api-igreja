# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
require 'faker'

enderecos = []
for i in 0..14
  enderecos[i] = {
    rua: 'Rua ' + Faker::Address.street_name,
    numero: Faker::Address.building_number.to_i,
    bairro: 'Baixada Verde',
    cidade: Faker::Address.city,
    pais: Faker::Address.country,
    estado: Faker::Address.state,
    cep: Faker::Address.zip_code,
  }
end
enderecosModel = Endereco.create(enderecos)

membros = []
for i in 0..10
  membros[i] = {
    nome: Faker::Name.name,
    telefone: Faker::PhoneNumber.cell_phone,
    email: Faker::Internet.email,
    endereco: enderecosModel[i]
  }
end

membrosModel = Membro.create(membros)

congregacoes = []
for i in 0..10
  congregacoes[i] = {
    nome: Faker::BossaNova.song,
    tipo: ['sede', 'filial'].slice(rand(0..1), 1)
  }
end

congregacoesModel = Congregacao.create(congregacoes)


membro_i = 0
endereco_i = 11
igrejas = Igreja.create([
  {
    nome: 'De Deus',
    secretario: membrosModel[membro_i += 1],
    tesoureiro: membrosModel[membro_i += 1],
    lider: membrosModel[membro_i += 1],
    endereco: enderecosModel[endereco_i += 1],
    membros: membrosModel.slice(membro_i-3, 5),
    congregacoes: congregacoesModel.slice(membro_i-3, rand(1..3))
  },
  {
    nome: 'Do Pais',
    secretario: membrosModel[membro_i += 1],
    tesoureiro: membrosModel[membro_i += 1],
    lider: membrosModel[membro_i += 1],
    endereco: enderecosModel[endereco_i += 1],
    membros: membrosModel.slice(membro_i-3, 5),
    congregacoes: congregacoesModel.slice(membro_i-3, rand(1..3))
  },
  {
    nome: 'A Global',
    secretario: membrosModel[membro_i += 1],
    tesoureiro: membrosModel[membro_i += 1],
    lider: membrosModel[membro_i += 1],
    endereco: enderecosModel[endereco_i += 1],
    membros: membrosModel.slice(membro_i-3, 5),
    congregacoes: congregacoesModel.slice(membro_i-3, rand(1..3))
  },
  {
    nome: 'Dos Varios',
    secretario: membrosModel[membro_i += 1],
    tesoureiro: membrosModel[membro_i += 1],
    lider: membrosModel[membro_i += 1],
    endereco: enderecosModel[endereco_i += 1],
    membros: membrosModel.slice(membro_i-3, 5),
    congregacoes: congregacoesModel.slice(membro_i-3, rand(1..3))
  }
])
