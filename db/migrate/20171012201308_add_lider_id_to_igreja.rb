class AddLiderIdToIgreja < ActiveRecord::Migration[5.0]
  def change
    add_reference :igrejas, :lider, reference: :membro, index: true
  end
end
