class CreateEnderecos < ActiveRecord::Migration[5.0]
  def change
    create_table :enderecos do |t|
      t.string :rua
      t.integer :numero
      t.string :bairro
      t.string :cidade
      t.string :estado
      t.string :pais
      t.string :cep

      t.timestamps
    end
  end
end
