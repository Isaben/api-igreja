class AddCongregacaoToMembro < ActiveRecord::Migration[5.0]
  def change
    add_reference :membros, :congregacao, reference: :congregacao, index: true
  end
end
