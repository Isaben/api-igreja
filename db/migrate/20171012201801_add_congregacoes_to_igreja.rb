class AddCongregacoesToIgreja < ActiveRecord::Migration[5.0]
  def change
    add_reference :igrejas, :congregacoes, references: :congregacoes, index: true
  end
end
