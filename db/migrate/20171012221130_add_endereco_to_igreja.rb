class AddEnderecoToIgreja < ActiveRecord::Migration[5.0]
  def change
    add_reference :igrejas, :endereco, reference: :endereco, index: true
  end
end
