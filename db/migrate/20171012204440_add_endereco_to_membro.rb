class AddEnderecoToMembro < ActiveRecord::Migration[5.0]
  def change
    add_reference :membros, :endereco, reference: :endereco, index: true
  end
end
