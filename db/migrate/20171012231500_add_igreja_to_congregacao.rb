class AddIgrejaToCongregacao < ActiveRecord::Migration[5.0]
  def change
    add_reference :congregacoes, :igreja, reference: :igreja, index: true
  end
end
