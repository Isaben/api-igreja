class AddIgrejaToMembro < ActiveRecord::Migration[5.0]
  def change
    add_reference :membros, :igreja, reference: :igreja, index: true
  end
end
