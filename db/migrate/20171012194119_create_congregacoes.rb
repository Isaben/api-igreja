class CreateCongregacoes < ActiveRecord::Migration[5.0]
  def change
    create_table :congregacoes do |t|
      t.string :nome
      t.string :tipo

      t.timestamps
    end
  end
end
