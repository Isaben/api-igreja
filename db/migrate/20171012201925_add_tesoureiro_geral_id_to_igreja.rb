class AddTesoureiroGeralIdToIgreja < ActiveRecord::Migration[5.0]
  def change
    add_reference :igrejas, :tesoureiro, reference: :membro, index: true
  end
end
