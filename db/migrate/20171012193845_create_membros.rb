class CreateMembros < ActiveRecord::Migration[5.0]
  def change
    create_table :membros do |t|
      t.string :nome
      t.string :funcao
      t.string :telefone
      t.string :email

      t.timestamps
    end
  end
end
