class AddSecretarioGeralIdToIgreja < ActiveRecord::Migration[5.0]
  def change
    add_reference :igrejas, :secretario, index: true, foreign_key: {to_table: :membros}
  end
end
