class Igreja < ApplicationRecord
  has_many :membros
  belongs_to :lider, class_name: "Membro"
  belongs_to :secretario, class_name: "Membro"
  belongs_to :tesoureiro, class_name: "Membro"
  belongs_to :endereco
  has_many :congregacoes, class_name: "Congregacao"
end
