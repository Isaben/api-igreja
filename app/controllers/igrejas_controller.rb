class IgrejasController < ApplicationController
  before_action :set_igreja, only: [:show, :update, :destroy]

  # GET /igrejas
  def index
    @igrejas = Igreja.all

    render json: @igrejas
  end

  # GET /igrejas/1
  def show
    render json: @igreja
  end

  # POST /igrejas
  def create
    @igreja = Igreja.new(igreja_params)
    if not igreja_params[:endereco_id] and igreja_params[:endereco]
      @igreja.endereco = Endereco.create(igreja_params[:endereco])
    end

    if @igreja.save
      render json: @igreja, status: :created, location: @igreja
    else
      render json: @igreja.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /igrejas/1
  def update
    if @igreja.update(igreja_params)
      render json: @igreja
    else
      render json: @igreja.errors, status: :unprocessable_entity
    end
  end

  # DELETE /igrejas/1
  def destroy
    @igreja.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_igreja
      @igreja = Igreja.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def igreja_params
      params.require(:igreja).permit(:nome, :lider_id, :tesoureiro_id, :secretario_id, :endereco_id)
    end
end
