class CongregacaosController < ApplicationController
  before_action :set_congregaco, only: [:show, :update, :destroy]

  # GET /congregacoes
  def index
    @congregacoes = Congregacao.all

    render json: @congregacoes
  end

  # GET /congregacoes/1
  def show
    render json: @congregaco
  end

  # POST /congregacoes
  def create
    @congregaco = Congregacao.new(congregaco_params)

    if @congregaco.save
      render json: @congregaco, status: :created, location: @congregaco
    else
      render json: @congregaco.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /congregacoes/1
  def update
    if @congregaco.update(congregaco_params)
      render json: @congregaco
    else
      render json: @congregaco.errors, status: :unprocessable_entity
    end
  end

  # DELETE /congregacoes/1
  def destroy
    @congregaco.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_congregaco
      @congregaco = Congregacao.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def congregaco_params
      params.require(:congregaco).permit(:nome, :tipo)
    end
end
