require 'test_helper'

class IgrejasControllerTest < ActionDispatch::IntegrationTest
  setup do
    @igreja = igrejas(:one)
  end

  test "should get index" do
    get igrejas_url, as: :json
    assert_response :success
  end

  test "should create igreja" do
    assert_difference('Igreja.count') do
      post igrejas_url, params: { igreja: { nome: @igreja.nome } }, as: :json
    end

    assert_response 201
  end

  test "should show igreja" do
    get igreja_url(@igreja), as: :json
    assert_response :success
  end

  test "should update igreja" do
    patch igreja_url(@igreja), params: { igreja: { nome: @igreja.nome } }, as: :json
    assert_response 200
  end

  test "should destroy igreja" do
    assert_difference('Igreja.count', -1) do
      delete igreja_url(@igreja), as: :json
    end

    assert_response 204
  end
end
