require 'test_helper'

class CongregacaosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @congregaco = congregacoes(:one)
  end

  test "should get index" do
    get congregacoes_url, as: :json
    assert_response :success
  end

  test "should create congregaco" do
    assert_difference('Congregacao.count') do
      post congregacoes_url, params: { congregaco: { nome: @congregaco.nome, tipo: @congregaco.tipo } }, as: :json
    end

    assert_response 201
  end

  test "should show congregaco" do
    get congregaco_url(@congregaco), as: :json
    assert_response :success
  end

  test "should update congregaco" do
    patch congregaco_url(@congregaco), params: { congregaco: { nome: @congregaco.nome, tipo: @congregaco.tipo } }, as: :json
    assert_response 200
  end

  test "should destroy congregaco" do
    assert_difference('Congregacao.count', -1) do
      delete congregaco_url(@congregaco), as: :json
    end

    assert_response 204
  end
end
